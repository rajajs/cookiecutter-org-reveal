Cookiecutter for org-reveal 
---------------------------

What is this?
=============

This is a [cookiecutter](https://github.com/audreyr/cookiecutter) template for making [reveal.js]() presentations using [org-mode]() and [org-reveal]().

Why should you use this?
========================

You can quickly start writing your first reveal.js presentation with org mode. Sample slides illustrating the common layouts are included. This is intended for those familiar with emacs and org-mode.  

How to use
===========

 1. Install cookiecutter.

   ```pip install cookiecutter```

 2. Make a directory for the presentation and navigate to it

    ```mkdir ~/reveal_talk```
    ```cd ~/reveal_talk```

 3. Use cookiecutter with this template to prepare the files. Answer the questions / pick your choice.

    ```cookiecutter gl:rajajs/cookiecutter-org-reveal```

 4. Your files are ready. Edit the org file in emacs. Make sure you have org-reveal installed and set up. Then use org export to export as reveal presentation. Modify local.css as required. For a portable version of the presentation, change reveal_single_file to 1 in the options at the top of the org file to export as a single file.  


Further reading
===============

 1. https://github.com/yjwen/org-reveal
 2. http://nwidger.github.io/blog/post/making-a-reveal.js-presentation-with-org-reveal/
